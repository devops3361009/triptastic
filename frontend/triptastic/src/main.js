import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Accueil from './pages/AccueilComponent.vue'
import Destination from './pages/DestinationComponent.vue'
import Activite from './pages/ActiviteComponent.vue'
import Contact from './pages/ContactComponent.vue'
import DestinationDetails from './pages/DestinationDetails.vue'
import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:5000'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Accueil, name: 'Accueil' },
    { path: '/destination', component: Destination, name: 'Destination' },
    { path: '/destination/:id', component: DestinationDetails, name: 'DestinationDetails' },
    { path: '/activite', component: Activite, name: 'Activite' },
    { path: '/contact', component: Contact, name: 'Contact' }
  ]
})

new Vue({
  router,
  render: (h) => h(App)
}).$mount('#app')
