const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' // Remplacez "nom-du-repertoire" par le nom réel du répertoire de déploiement
    : '/',
  transpileDependencies: true
  
})