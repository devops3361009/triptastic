from src.restaurant import repoRestaurant

def informationsRestaurant(db, id_restaurant):
    restaurant = repoRestaurant.informationsRestaurant(db, id_restaurant)
    return {
        'id_restaurant': restaurant[0],
        'nom_restaurant': restaurant[1],
        'type_cuisine': restaurant[2],
        'budget': restaurant[3],
        'id_destination': restaurant[4]
    }


def restaurantsPourDestination(db, id_destination):
    restaurants = repoRestaurant.restaurantsPourDestination(db, id_destination)
    result = []
    for r in restaurants:
        result.append({
            'id_restaurant': r[0],
            'nom_restaurant': r[1],
            'type_cuisine': r[2],
            'budget': r[3],
            'id_destination': r[4]
        })
    return result


def getRestaurantImages(db, id_restaurant):
    images = repoRestaurant.getRestaurantImages(db, id_restaurant)
    result = []
    for image in images:
        result.append({
            'id_image': image[0],
            'nom_image': image[1],
            'chemin_image': image[2],
            'id_destination': image[3],
            'id_activite': image[4],
            'id_restaurant': image[5]
        })
    return result
