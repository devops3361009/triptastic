def informationsRestaurant(db, id_restaurant):
    c = db.cursor()
    c.execute("""
        SELECT * FROM restaurant
        WHERE id_restaurant = %s;
    """, (id_restaurant,))
    return c.fetchone()

def restaurantsPourDestination(db, id_destination):
    c = db.cursor()
    c.execute("""
        SELECT * FROM restaurant
        WHERE id_destination = %s;
    """, (id_destination,))
    return c.fetchall()

def getRestaurantImages(db, id_restaurant):
    c = db.cursor()
    c.execute("""
        SELECT * FROM image WHERE id_restaurant = %s
    """, (id_restaurant,))
    return c.fetchall()
