
def destinationsTrierParNom(db):
    db.reconnect() 
    c = db.cursor()
    #c.execute("USE travel_db2")
    c.execute("""
    SELECT destination.*, image.chemin_image FROM destination INNER JOIN image ON destination.id_destination = image.id_destination;
    """)
    #order by nom_destination asc
    return c.fetchall()

def informationsDestination(db, id_destination):
    db.reconnect() 
    c = db.cursor()
    c.execute("""
        SELECT destination.*, image.chemin_image
        FROM destination 
        INNER JOIN image ON destination.id_destination = image.id_destination
        WHERE destination.id_destination = %s;
    """, (id_destination,))
    return c.fetchall()

def destinationsParContinent(db, continent):
    db.reconnect() 
    c = db.cursor()
    c.execute("""
        SELECT * FROM destination 
        WHERE continent = %s;
    """, (continent,))
    return c.fetchall()

def destinationsJolies(db):
    db.reconnect()
    c = db.cursor()
    c.execute("""
        SELECT d.id_destination, d.nom_destination, d.pays, d.continent, d.climat, d.description
        FROM destination d
        JOIN activite a ON d.id_destination = a.id_destination
        WHERE a.categorie = 'paysage'
        GROUP BY d.id_destination
        HAVING COUNT(DISTINCT a.id_activite) >= 5;
    """)
    return c.fetchall()

def destinationsCulturelles(db):
    db.reconnect() 
    c = db.cursor()
    c.execute("""
        SELECT d.id_destination, d.nom_destination, d.pays, d.continent, d.climat, d.description
        FROM destination d
        JOIN activite a ON d.id_destination = a.id_destination
        WHERE a.categorie = 'culturelle'
        GROUP BY d.id_destination
        HAVING COUNT(DISTINCT a.id_activite) >= 5;
    """)
    return c.fetchall()

def rechercheAvancee(db, climat, continent, type_activite):
    db.reconnect() 
    c = db.cursor()
    query = "SELECT d.id_destination, d.nom_destination, d.pays, d.continent, d.climat, d.description FROM destination d"
    conditions = []

    if climat:
        conditions.append("d.climat = %s")

    if continent:
        conditions.append("d.continent = %s")

    if type_activite:
        conditions.append("a.categorie = %s")

    if conditions:
        query += " JOIN activite a ON d.id_destination = a.id_destination WHERE " + " AND ".join(conditions)

    c.execute(query, [climat, continent, type_activite])
    return c.fetchall()

def getDestinationImages(db, id_destination):
    c = db.cursor()
    c.execute("""
        SELECT * FROM image WHERE id_destination = %s
    """, (id_destination,))
    return c.fetchall()
