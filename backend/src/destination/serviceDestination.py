from src.destination import repoDestination


def destinationsTrierParNom(db):
    result = []
    for d in repoDestination.destinationsTrierParNom(db):
         result.append({
             'id_destination': d[0],
             'nom_destination': d[1],
             'pays': d[2],
             'continent': d[3],
             'climat': d[4],
             'description': d[5],
             'image':d[6]
         })
    return result


def informationsDestination(db, id_destination):
    destination = repoDestination.informationsDestination(db, id_destination)[0] 
    return {
        'id_destination': destination[0],
        'nom_destination': destination[1],
        'pays': destination[2],
        'continent': destination[3],
        'climat': destination[4],
        'description': destination[5],
        'image': destination[6]
    }


def destinationsParContinent(db, continent):
    result = []
    for d in repoDestination.destinationsParContinent(db, continent):
        result.append({
            'id_destination': d[0],
            'nom_destination': d[1],
            'pays': d[2],
            'continent': d[3],
            'climat': d[4],
            'description': d[5]
        })
    return result


def destinationsJolies(db):
    destinations = repoDestination.destinationsJolies(db)
    result = []
    for d in destinations:
        result.append({
            'id_destination': d[0],
            'nom_destination': d[1],
            'pays': d[2],
            'continent': d[3],
            'climat': d[4],
            'description': d[5]
        })
    return result


def destinationsCulturelles(db):
    destinations = repoDestination.destinationsCulturelles(db)
    result = []
    for d in destinations:
        result.append({
            'id_destination': d[0],
            'nom_destination': d[1],
            'pays': d[2],
            'continent': d[3],
            'climat': d[4],
            'description': d[5]
        })
    return result


def rechercheAvancee(db, climat, continent, type_activite):
    destinations = repoDestination.rechercheAvancee(db, climat, continent, type_activite)
    result = []
    for d in destinations:
        result.append({
            'id_destination': d[0],
            'nom_destination': d[1],
            'pays': d[2],
            'continent': d[3],
            'climat': d[4],
            'description': d[5]
        })
    return result


def getDestinationImages(db, id_destination):
    images = repoDestination.getDestinationImages(db, id_destination)
    result = []
    for image in images:
        result.append({
            'id_image': image[0],
            'nom_image': image[1],
            'chemin_image': image[2],
            'id_destination': image[3],
            'id_activite': image[4],
            'id_restaurant': image[5]
        })
    return result
