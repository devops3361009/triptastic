def toutesLesActivites(db):
    db.reconnect()
    c = db.cursor()
    c.execute("""
        SELECT activite.*, image.chemin_image
        FROM activite
        INNER JOIN image ON activite.id_activite = image.id_activite;
    """)
    return c.fetchall()


def informationsActivite(db, id_activite):
    db.reconnect()
    c = db.cursor()
    c.execute("""
        SELECT activite.*, image.chemin_image
        FROM activite
        INNER JOIN image ON activite.id_activite = image.id_activite
        WHERE activite.id_activite = %s;
    """, (id_activite,))
    return c.fetchall()


def activitesParDestination(db, id_destination):
    db.reconnect()
    c = db.cursor()
    c.execute("""
        SELECT activite.*, image.chemin_image
        FROM activite
        INNER JOIN image ON activite.id_activite = image.id_activite
        WHERE activite.id_destination = %s;
    """, (id_destination,))
    return c.fetchall()


def activitesParTypeDestination(db, id_destination, type_activite):
    db.reconnect()
    c = db.cursor()
    c.execute("""
        SELECT * FROM activite
        WHERE id_destination = %s AND categorie = %s;
    """, (id_destination, type_activite))
    return c.fetchall()


def getActiviteImages(db, id_activite):
    db.reconnect()
    c = db.cursor()
    c.execute("""
        SELECT * FROM image WHERE id_activite = %s
    """, (id_activite,))
    return c.fetchall()
