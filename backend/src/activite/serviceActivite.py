from src.activite import repoActivite


def toutesLesActivites(db):
    result = []
    for a in repoActivite.toutesLesActivites(db):
        result.append({
            'id_activite': a[0],
            'nom_activite': a[1],
            'categorie': a[2],
            'saisonnalite': a[3],
            'id_destination': a[4],
            'image': a[5]
        })
    return result


def informationsActivite(db, id_activite):
    activite = repoActivite.informationsActivite(db, id_activite)[0]
    return {
        'id_activite': activite[0],
        'nom_activite': activite[1],
        'categorie': activite[2],
        'saisonnalite': activite[3],
        'id_destination': activite[4],
        'image': activite[5] 
    }


def activitesParDestination(db, id_destination):
    result = []
    for a in repoActivite.activitesParDestination(db, id_destination):
        result.append({
            'id_activite': a[0],
            'nom_activite': a[1],
            'categorie': a[2],
            'saisonnalite': a[3],
            'id_destination': a[4],
            'image': a[5]
        })
    return result


def activitesParTypeDestination(db, id_destination, type_activite):
    result = []
    for a in repoActivite.activitesParTypeDestination(db, id_destination, type_activite):
        result.append({
            'id_activite': a[0],
            'nom_activite': a[1],
            'categorie': a[2],
            'saisonnalite': a[3],
            'id_destination': a[4]
        })
    return result

def getActiviteImages(db, id_activite):
    images = repoActivite.getActiviteImages(db, id_activite)
    result = []
    for image in images:
        result.append({
            'id_image': image[0],
            'nom_image': image[1],
            'chemin_image': image[2],
            'id_destination': image[3],
            'id_activite': image[4],
            'id_restaurant': image[5]
        })
    return result
