from . import creds as CREDS
import mysql.connector

def connect_to_db():
    mydb = None
    try:
        mydb = mysql.connector.connect(
            host=CREDS.DBHOST,
            user=CREDS.DBUSER,
            password=CREDS.DBPASSWORD,
            database=CREDS.DBNAME,
            auth_plugin='mysql_native_password' 
            )
    except mysql.connector.Error as err:
        print("Something went wrong when connecting to the db: {}".format(err))

    if mydb is not None:
        return mydb
    else:
        print("Error connecting to the database")
        exit(1)

