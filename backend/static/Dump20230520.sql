-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: localhost    Database: travel_db
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activite`
--

DROP TABLE IF EXISTS `activite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activite` (
  `id_activite` int NOT NULL AUTO_INCREMENT,
  `nom_activite` varchar(80) NOT NULL,
  `categorie` varchar(40) NOT NULL,
  `saisonnalite` varchar(40) NOT NULL,
  `id_destination` int NOT NULL,
  PRIMARY KEY (`id_activite`),
  KEY `id_destination` (`id_destination`),
  CONSTRAINT `Activite_ibfk_1` FOREIGN KEY (`id_destination`) REFERENCES `destination` (`id_destination`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activite`
--

LOCK TABLES `activite` WRITE;
/*!40000 ALTER TABLE `activite` DISABLE KEYS */;
/*!40000 ALTER TABLE `activite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avis`
--

DROP TABLE IF EXISTS `avis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `avis` (
  `id_avis` int NOT NULL AUTO_INCREMENT,
  `id_destination` int NOT NULL,
  `id_restaurant` int DEFAULT NULL,
  `id_activite` int DEFAULT NULL,
  `id_utilisateur` int NOT NULL,
  `note` int NOT NULL,
  `commentaire` text,
  PRIMARY KEY (`id_avis`),
  KEY `id_destination` (`id_destination`),
  KEY `id_restaurant` (`id_restaurant`),
  KEY `id_activite` (`id_activite`),
  KEY `id_utilisateur` (`id_utilisateur`),
  CONSTRAINT `Avis_ibfk_1` FOREIGN KEY (`id_activite`) REFERENCES `activite` (`id_activite`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Avis_ibfk_2` FOREIGN KEY (`id_destination`) REFERENCES `destination` (`id_destination`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Avis_ibfk_3` FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id_restaurant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Avis_ibfk_4` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avis`
--

LOCK TABLES `avis` WRITE;
/*!40000 ALTER TABLE `avis` DISABLE KEYS */;
/*!40000 ALTER TABLE `avis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination`
--

DROP TABLE IF EXISTS `destination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `destination` (
  `id_destination` int NOT NULL AUTO_INCREMENT,
  `nom_destination` varchar(80) NOT NULL,
  `pays` varchar(40) NOT NULL,
  `continent` varchar(40) NOT NULL,
  `climat` varchar(40) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_destination`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination`
--

LOCK TABLES `destination` WRITE;
/*!40000 ALTER TABLE `destination` DISABLE KEYS */;
INSERT INTO `destination` VALUES (1,'Paris','France','Europe','tempéré','Découvrez Paris, la ville des lumières et de l\'amour. Promenez-vous le long de la Seine et admirez la Tour Eiffel, le Louvre et Notre-Dame. Imprégnez-vous de l\'histoire en visitant le Palais du Louvre et le Château de Versailles. Plongez dans l\'atmosphère bohème de Montmartre et de Saint-Germain-des-Prés. Goûtez aux délices culinaires français tels que les croissants, les fromages et les pâtisseries. Explorez la scène artistique dynamique de la ville avec ses musées, galeries d\'art et théâtres. Perdez-vous dans les ruelles pavées et profitez de l\'ambiance romantique des cafés parisiens. Réservez dès maintenant votre voyage à Paris et laissez-vous séduire par cette ville magique.'),(2,'Rome','Italie','Europe','tempéré','Découvrez Rome, la ville éternelle, empreinte d\'histoire, de culture et de beauté. Explorez les vestiges majestueux de l\'Empire romain, comme le Colisée et le Forum romain. Plongez dans l\'atmosphère pittoresque des ruelles animées, des places charmantes et des fontaines célèbres. Savourez la cuisine italienne authentique, des pâtes savoureuses aux pizzas délicieuses. Explorez les musées fascinants, des chefs-d\'œuvre artistiques de la chapelle Sixtine au Vatican aux trésors antiques du Musée du Vatican. Flânez dans les jardins paisibles de la Villa Borghèse et découvrez le quartier bohème de Trastevere. Imprégnez-vous de la dolce vita romaine en vous mêlant aux habitants dans les marchés animés et les boutiques élégantes. Laissez-vous charmer par l\'hospitalité chaleureuse des Romains. Réservez dès maintenant votre voyage et laissez-vous envoûter par la magie intemporelle de Rome.'),(3,'Bali','Indonésie','Asie','tropical','Découvrez Bali, un véritable paradis tropical où la beauté naturelle se mêle à une culture envoûtante. Plages de sable fin, rizières en terrasses et temples sacrés vous attendent. Plongez dans les eaux turquoise pour explorer des récifs coralliens colorés ou surfez sur les vagues légendaires de l\'île. Bali offre également une cuisine raffinée, des spas de renommée mondiale et une ambiance spirituelle. Découvrez l\'artisanat local dans les marchés animés et laissez-vous séduire par l\'hospitalité chaleureuse du peuple balinais. Trouvez un équilibre entre détente et aventure, entre exploration culturelle et reconnexion avec la nature. Bali vous invite à plonger dans son harmonie enchanteresse. Réservez dès maintenant votre voyage et laissez-vous envoûter par la magie de Bali, où chaque moment est une invitation à la découverte et à la sérénité.'),(4,'Lisbonne','Portugal','Europe','tempéré','Découvrez Lisbonne, la perle du Portugal, où l\'histoire rencontre la modernité dans une harmonie envoûtante. Explorez les ruelles pittoresques de l\'Alfama et profitez des vues panoramiques depuis le château de São Jorge. Admirez l\'architecture magnifique des azulejos et des bâtiments colorés. Lisbonne regorge de trésors culturels tels que le monastère des Hiéronymites et la tour de Belém. Goûtez à la cuisine portugaise authentique et savourez les fameux pasteis de nata. Promenez-vous le long du Tage et admirez les vues sur le pont du 25 avril et la statue du Christ Roi. Lisbonne offre une atmosphère chaleureuse, imprégnée de saudade, un sentiment mélancolique doux. Découvrez la richesse culturelle, l\'hospitalité des habitants et laissez-vous charmer par le fado, la musique traditionnelle portugaise. Réservez dès maintenant votre voyage à Lisbonne et plongez-vous dans la beauté et l\'authenticité de cette ville fascinante.'),(5,'Lima','Pérou','Amérique','subtropical','Découvrez Lima, la capitale vibrante du Pérou, où l\'histoire rencontre une scène culinaire renommée et des paysages côtiers époustouflants. Plongez-vous dans le centre historique, avec ses églises baroques et ses places animées. Laissez-vous séduire par la cuisine péruvienne, réputée mondialement pour ses délices tels que le ceviche et le lomo saltado. Explorez les musées fascinants de la ville, témoins de la richesse culturelle du pays. Détendez-vous sur les magnifiques plages de la Costa Verde, avec des activités nautiques passionnantes. Imprégnez-vous de l\'énergie et de la diversité culturelle de Lima. Réservez dès maintenant votre voyage à Lima et découvrez une métropole envoûtante, où l\'histoire, la gastronomie et la beauté naturelle se rencontrent pour créer une expérience unique.');
/*!40000 ALTER TABLE `destination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `image` (
  `id_image` int NOT NULL AUTO_INCREMENT,
  `nom_image` varchar(40) NOT NULL,
  `chemin_image` varchar(255) NOT NULL,
  `id_destination` int DEFAULT NULL,
  `id_activite` int DEFAULT NULL,
  `id_restaurant` int DEFAULT NULL,
  PRIMARY KEY (`id_image`),
  KEY `id_destination` (`id_destination`),
  KEY `id_activite` (`id_activite`),
  KEY `id_restaurant` (`id_restaurant`),
  CONSTRAINT `Image_ibfk_1` FOREIGN KEY (`id_activite`) REFERENCES `activite` (`id_activite`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Image_ibfk_2` FOREIGN KEY (`id_destination`) REFERENCES `destination` (`id_destination`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Image_ibfk_3` FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id_restaurant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,'paris','https://zupimages.net/up/23/20/6vhy.jpg',1,NULL,NULL),(2,'rome','https://zupimages.net/up/23/20/jm0z.jpg',2,NULL,NULL),(3,'bali','https://zupimages.net/up/23/20/lzdt.jpg',3,NULL,NULL),(4,'lisbonne','https://zupimages.net/up/23/20/tdkd.jpg',4,NULL,NULL),(5,'lima','https://zupimages.net/up/23/20/qt5b.jpg',5,NULL,NULL);
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restaurant` (
  `id_restaurant` int NOT NULL AUTO_INCREMENT,
  `nom_restaurant` varchar(80) NOT NULL,
  `type_cuisine` varchar(40) NOT NULL,
  `budget` varchar(20) NOT NULL,
  `id_destination` int NOT NULL,
  PRIMARY KEY (`id_restaurant`),
  KEY `id_destination` (`id_destination`),
  CONSTRAINT `Restaurant_ibfk_1` FOREIGN KEY (`id_destination`) REFERENCES `destination` (`id_destination`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant`
--

LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utilisateur` (
  `id_utilisateur` int NOT NULL AUTO_INCREMENT,
  `nom_utilisateur` varchar(80) NOT NULL,
  `adresse_email` varchar(80) NOT NULL,
  `mot_de_passe` varchar(80) NOT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-20 22:19:50
