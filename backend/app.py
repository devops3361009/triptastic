from flask import Flask, jsonify, request
from database.db import connect_to_db
from database.creds import DBNAME
from flask_cors import CORS

from src.destination import serviceDestination
from src.activite import serviceActivite
from src.restaurant import serviceRestaurant

app = Flask(__name__)
CORS(app)

db = connect_to_db()

@app.route("/")
def index():
    return "Hello world!"



@app.route("/destination/trier_par_nom")
def destinationTrierParNom():
    destinations = serviceDestination.destinationsTrierParNom(db)
    return jsonify(destinations)

@app.route("/destination/<id_destination>")
def informationsDestination(id_destination):
    destination = serviceDestination.informationsDestination(db, id_destination)
    return jsonify(destination)

@app.route("/destination/par_continent/<continent>")
def destinationParContinent(continent):
    destinations = serviceDestination.destinationsParContinent(db, continent)
    return jsonify(destinations)

@app.route("/destinations/jolies")
def destinationsJoliess():
    destinations = serviceDestination.destinationsJolies(db)
    return jsonify(destinations)

@app.route("/destinations/culturelles")
def destinationsCulturelles():
    destinations = serviceDestination.destinationsCulturelles(db)
    return jsonify(destinations)

@app.route("/destinations/recherche_avancee")
def rechercheAvancee():
    climat = request.args.get('climat')
    budget = request.args.get('continent')
    activites = request.args.getlist('activites')

    destinations = serviceDestination.rechercheAvancee(db, climat, budget, activites)
    return jsonify(destinations)


@app.route("/destinations/<id_destination>/images")
def getDestinationImages(id_destination):
    images = serviceDestination.getDestinationImages(db, id_destination)
    return jsonify(images)





@app.route("/activites")
def toutesLesActivites():
    activites = serviceActivite.toutesLesActivites(db)
    return jsonify(activites)


@app.route("/activite/<id_activite>")
def informationsActivite(id_activite):
    activite = serviceActivite.informationsActivite(db, id_activite)
    return jsonify(activite)

@app.route("/activites/<id_destination>")
def activitesParDestination(id_destination):
    activites = serviceActivite.activitesParDestination(db, id_destination)
    return jsonify(activites)

@app.route("/activites/<id_destination>/<type_activite>")
def activitesParTypeDestination(id_destination, type_activite):
    activites = serviceActivite.activitesParTypeDestination(db, id_destination, type_activite)
    return jsonify(activites)

@app.route("/activites/<id_activite>/images")
def getActiviteImages(id_activite):
    images = serviceActivite.getImagesByActivite(db, id_activite)
    return jsonify(images)




@app.route("/restaurant/<id_restaurant>")
def informationsRestaurant(id_restaurant):
    restaurant = serviceRestaurant.informationsRestaurant(db, id_restaurant)
    return jsonify(restaurant)

@app.route("/restaurants/<id_destination>")
def restaurantsPourDestination(id_destination):
    restaurants = serviceRestaurant.restaurantsPourDestination(db, id_destination)
    return jsonify(restaurants)

@app.route("/restaurants/<id_restaurant>/images")
def getRestaurantImages(id_restaurant):
    images = serviceRestaurant.getRestaurantImages(db, id_restaurant)
    return jsonify(images)


if __name__ == '__main__':
    app.run()